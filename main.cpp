
#include <allegro.h>
#ifndef linux
#include <winalleg.h>
#include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COLOR_BLACK 			makecol(0,0,0)
#define COLOR_WHITE 			makecol(0xff,0xff,0xff)
#define COLOR_RED 				makecol(0xff,0,0)
#define COLOR_GREEN 			makecol(0,0xff,0)

#define TARGET_WIDTH 			100/2	//bitmap w
#define TARGET_HEIGHT 			144/2	//bitmap h
#define TARGET_MAX_NUM			10	//total num target objects
#define TARGET_TOTAL_PERSONS	5	//total num target people
#define TARGET_PERSON_TONY		0	//list of target people
#define TARGET_PERSON_GORDON	1
#define TARGET_PERSON_JOHN		2
#define TARGET_PERSON_JACK		3
#define TARGET_PERSON_DAVID		4

#define WEAPON_SCOPE_SIZE		3 	//radius of inner circle
#define WEAPON_SCOPE_RADIUS		12	//radius of outest circle
#define WEAPON_SCOPE_MINSPEED	5
#define WEAPON_SCOPE_MAXSPEED	12
#define WEAPON_SCOPE_COLOR		COLOR_GREEN
#define WEAPON_AQUIRE_COLOR		COLOR_RED
#define WEAPON_LASER_COLOR		COLOR_WHITE
#define WEAPON_LASER_POWER		128	//eg. 2 -> 50

void cls(void) //clear screen
{
	BITMAP *bmp = create_bitmap(SCREEN_W, SCREEN_H); //make bitmap in ram
	clear_bitmap(bmp); //zero memory
	blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);  //copy to screen
}
END_OF_FUNCTION(cls);

int quit(int ret = 0)
{
	allegro_exit();
	rest(200);
	exit(ret);
}
END_OF_FUNCTION(quit);

void splash(void)
{
	textout_centre(screen, font, "---[ vote labour ]--------", (SCREEN_W/2), (SCREEN_H/2)-40, COLOR_RED);
	textout_centre(screen, font, " 's' to start...", (SCREEN_W/2), (SCREEN_H/2)-20, COLOR_GREEN);
	textout_centre(screen, font, " '<arrows>' to aim...", (SCREEN_W/2), (SCREEN_H/2), COLOR_GREEN);
	textout_centre(screen, font, " '<space>' to fire...", (SCREEN_W/2), (SCREEN_H/2)+20, COLOR_GREEN);
	textout_centre(screen, font, " 'q' to end...", (SCREEN_W/2), (SCREEN_H/2)+40, COLOR_GREEN);
	textout_centre(screen, font, " 'esc' to exit...", (SCREEN_W/2), (SCREEN_H/2)+60, COLOR_GREEN);
	while(!key[KEY_S] && !key[KEY_ESC]) poll_keyboard();
	if(key[KEY_ESC]) { 	quit(0); }
}
END_OF_FUNCTION(splash);

class error
{
	const char *message;
	public:
		error(const char *message) { this->message = message; }
		const char *get_message(void) { return this->message; }
};

class object
{
	public:
		int x;
		int y;
		int w;
		int h;
		int x_prev;
		int y_prev;
		int x_flag;	//0 = going right (inc), 1 = going left (dec)
		int y_flag; //0 = going down (inc), 1 = going up (dec)
};

class target : public object
{
	public:

		int id; //person id. eg. TARGET_PERSON_TONY

	    BITMAP *pic1;
	    BITMAP *pic2;

		SAMPLE *audio_hit;
		SAMPLE *audio_destroy;

		target(int _x, int _y, int _w, int _h, int _id)
		{
			this->id = _id;
			//set parent properties
			this->x=_x;
			this->y=_y;
			this->w=_w;
			this->h=_h;
			this->x_prev=this->x;
			this->y_prev=this->y;
			this->x_flag=(rand()%2); //randomise direction
			this->y_flag=~this->x_flag;
		}

		~target(void)
		{
			//rectfill(screen, this->x, this->y, this->x+this->w, this->y+this->h, 0);
			play_sample(this->audio_destroy, 127, rand()%255, 1000, 0);
			rectfill(screen, this->x, this->y, this->x+TARGET_WIDTH, this->y+TARGET_HEIGHT, COLOR_BLACK);
			draw_sprite(screen, this->pic2, this->x, this->y);
			rest(33);
			rectfill(screen, this->x, this->y, this->x+TARGET_WIDTH, this->y+TARGET_HEIGHT, COLOR_BLACK);
		}
		int init(void)
		{
			try
			{
				//load person pic
				switch(this->id)
				{
					case TARGET_PERSON_TONY:
						this->pic1 = load_bitmap("bmp/tony.bmp", NULL);
						break;
					case TARGET_PERSON_GORDON:
						this->pic1 = load_bitmap("bmp/gordon.bmp", NULL);
						break;
					case TARGET_PERSON_JOHN:
						this->pic1 = load_bitmap("bmp/john.bmp", NULL);
						break;
					case TARGET_PERSON_JACK:
						this->pic1 = load_bitmap("bmp/jack.bmp", NULL);
						break;
					case TARGET_PERSON_DAVID:
						this->pic1 = load_bitmap("bmp/david.bmp", NULL);
						break;
				}
				if(this->pic1 == NULL) {
					throw new error("Failed loading bitmap PIC_$(NAME)");
				}

				//load splatted pic
				this->pic2 = load_bitmap("bmp/splat.bmp", NULL);
				if(this->pic2 == NULL) {
					throw new error("Failed loading bitmap PIC_SPLAT");
				}

				//load audio
				this->audio_hit = load_sample("./wav/hit.wav");
				if(this->audio_hit == NULL) {
					throw new error("Failed loading sample AUDIO_TARGET_HIT");
				}

				//load random death sample
				switch((rand()%5)+1)
				{
					case 1:
						this->audio_destroy = load_sample("wav/groan1.wav");
						break;
					case 2:
						this->audio_destroy = load_sample("wav/groan2.wav");
						break;
					case 3:
						this->audio_destroy = load_sample("wav/groan3.wav");
						break;
					case 4:
						this->audio_destroy = load_sample("wav/groan4.wav");
						break;
					case 5:
						this->audio_destroy = load_sample("wav/groan5.wav");
						break;
				}

				if(this->audio_destroy == NULL){
					throw new error("Failed loading sample AUDIO_DESTROY_X");
				}

			} catch(error *e) {
				allegro_message("Error: %s\n", e->get_message());
				quit(1);
			}

			return 0;
		}
		void move()
		{
			if(this->x_flag) //going right
			{
				if(this->x<(SCREEN_W - this->w)) { this->x_prev=this->x; this->x++; }
				else this->x_flag=0;
			}
			else // if(!this->x_flag)
			{
				if(this->x>0) { this->x_prev=this->x; this->x--; }
				else this->x_flag=1;
			}
			if(this->y_flag)
			{
				if(this->y<(SCREEN_H - this->h)) { this->y_prev=this->y; this->y++; }
				else this->y_flag=0;
			}
			else //if(!this->y_flag)
			{
				if(this->y>0) { this->y_prev=this->y; this->y--; }
				else this->y_flag=1;
			}
		}
		int is_hit(int a, int b)
		{
			if(	(a >= this->x) &&
				(b >= this->y) &&
				(a <= (this->x+this->w)) &&
				(b <= (this->y+this->h)))
			{
				play_sample(this->audio_hit, 127, 127, 1000, 0);
				return 1;
			}
			return 0;
		}
		int is_aquired(int a, int b)
		{
			if(	(a >= this->x) &&
				(b >= this->y) &&
				(a <= (this->x+this->w)) &&
				(b <= (this->y+this->h)))
			{
				return 1;
			}
			return 0;
		}
		void draw(void)
		{
			rectfill(screen, this->x_prev, this->y_prev, this->x_prev+TARGET_WIDTH, this->y_prev+TARGET_HEIGHT, COLOR_BLACK);
			draw_sprite(screen, this->pic1, this->x, this->y);
		}
};

class weapon : public object
{
	public:

		int scope_radius;

		int x_speed;
		int y_speed;

		int target_aquire; //target tracked
		signed int aquired_target; //index of target

		SAMPLE *audio_aquire;
		SAMPLE *audio_fire;

		weapon(void)
		{
			this->scope_radius=WEAPON_SCOPE_RADIUS;
			this->x=(rand()%(SCREEN_W-this->scope_radius));
			this->y=(rand()%(SCREEN_H-this->scope_radius));
			this->x_prev=this->x;
			this->y_prev=this->y;
			this->x_flag=0;
			this->y_flag=0;
			this->x_speed=WEAPON_SCOPE_MINSPEED;
			this->y_speed=WEAPON_SCOPE_MINSPEED;
			this->target_aquire=0;
			this->aquired_target=-1;
		}

		~weapon(void) { }

		int init(void)
		{
			try
			{
				this->audio_fire = load_sample("wav/fire.wav");
				if(this->audio_fire == NULL) {
					throw new error("Failed loading sample AUDIO_WEAPON_FIRE");
				}

				this->audio_aquire = load_sample("wav/aquire.wav");
				if(this->audio_aquire == NULL) {
					throw new error("Failed loading sample AUDIO_WEAPON_AQUIRE");
				}

			} catch(error *e) {
				allegro_message("Error: %s\n", e->get_message());
				quit(1);
			}

			return 0;
		}

		void aim_right(void)
		{
			this->x_flag=0;
			if(!this->x_speed) this->x_speed=WEAPON_SCOPE_MINSPEED;
			else if(this->x_speed < WEAPON_SCOPE_MAXSPEED) this->x_speed+=2;
			if(this->y_speed>WEAPON_SCOPE_MINSPEED) this->y_speed--;
		}
		void aim_left(void)
		{
			this->x_flag=1;
			if(!this->x_speed) this->x_speed=WEAPON_SCOPE_MINSPEED;
			else if(this->x_speed < WEAPON_SCOPE_MAXSPEED) this->x_speed+=2;
			if(this->y_speed>WEAPON_SCOPE_MINSPEED) this->y_speed--;
		}
		void aim_down(void)
		{
			this->y_flag=0;
			if(!this->y_speed) this->y_speed=WEAPON_SCOPE_MINSPEED;
			else if(this->y_speed < WEAPON_SCOPE_MAXSPEED) this->y_speed+=2;
			if(this->x_speed>WEAPON_SCOPE_MINSPEED) this->x_speed--;
		}
		void aim_up(void)
		{
			this->y_flag=1;
			if(!this->y_speed) this->y_speed=WEAPON_SCOPE_MINSPEED;
			else if(this->y_speed < WEAPON_SCOPE_MAXSPEED) this->y_speed+=2;
			if(this->x_speed>WEAPON_SCOPE_MINSPEED) this->x_speed--;
		}

		void shoot(void)
		{
			play_sample(audio_fire, 127, 127, 1000, 0);
			triangle(screen, (SCREEN_W/2)-(WEAPON_LASER_POWER/2), SCREEN_H, (SCREEN_W/2)+(WEAPON_LASER_POWER/2), SCREEN_H, this->x, this->y, WEAPON_LASER_COLOR);
			rest(25);
			triangle(screen, (SCREEN_W/2)-(WEAPON_LASER_POWER/2), SCREEN_H, (SCREEN_W/2)+(WEAPON_LASER_POWER/2), SCREEN_H, this->x, this->y, COLOR_BLACK);
		}
		int target_aquired(void)
		{
			return this->target_aquire;
		}
		void aquire_target(int index)
		{
			play_sample(audio_aquire, 127, 127, 1000, 0);
			this->target_aquire=1;
			this->aquired_target=index;
		}
		void unaquire_target(void)
		{
			this->target_aquire=0;
			this->aquired_target=-1;
		}
		void target_track(int a, int b)
		{
			this->x_prev=this->x;
			this->y_prev=this->y;
			this->x=a;
			this->y=b;
		}

		void refresh(void)
		{
			if(this->x_speed) this->x_speed--;
			if(this->y_speed) this->y_speed--;

			this->x_prev=this->x;
			if(!this->x_flag) this->x+=this->x_speed;
			if(this->x_flag) this->x-=this->x_speed;

			this->y_prev=this->y;
			if(!this->y_flag) this->y+=this->y_speed;
			if(this->y_flag) this->y-=this->y_speed;

			if(this->x < 0) this->x = 0;
			if(this->x >= SCREEN_W) this->x = SCREEN_W;
			if(this->y < 0) this->y = 0;
			if(this->y >= SCREEN_H) this->y = SCREEN_H;
		}

		void draw(void)
		{
			//clear previous
			circle(screen, this->x_prev, this->y_prev, this->scope_radius, COLOR_BLACK);
			circle(screen, this->x_prev, this->y_prev, WEAPON_SCOPE_SIZE, COLOR_BLACK);
			vline(screen, this->x_prev, this->y_prev-this->scope_radius, this->y_prev-WEAPON_SCOPE_SIZE, COLOR_BLACK);
			vline(screen, this->x_prev, this->y_prev+WEAPON_SCOPE_SIZE, this->y_prev+this->scope_radius, COLOR_BLACK);
			hline(screen, this->x_prev-this->scope_radius, this->y_prev, this->x_prev-WEAPON_SCOPE_SIZE, COLOR_BLACK);
			hline(screen, this->x_prev+WEAPON_SCOPE_SIZE, this->y_prev, this->x_prev+this->scope_radius, COLOR_BLACK);

			//create new
			circle(screen, this->x_prev, this->y_prev, this->scope_radius+2, COLOR_BLACK);
			circle(screen, this->x_prev, this->y_prev, this->scope_radius+4, COLOR_BLACK);
			if(this->target_aquire)
			{
				circle(screen, this->x, this->y, this->scope_radius+2, WEAPON_AQUIRE_COLOR);
				circle(screen, this->x, this->y, this->scope_radius+4, WEAPON_AQUIRE_COLOR);
			}

			circle(screen, this->x, this->y, this->scope_radius, WEAPON_SCOPE_COLOR);
			circle(screen, this->x, this->y, WEAPON_SCOPE_SIZE, WEAPON_SCOPE_COLOR);
			vline(screen, this->x, this->y-this->scope_radius, this->y-WEAPON_SCOPE_SIZE, WEAPON_SCOPE_COLOR);
			vline(screen, this->x, this->y+WEAPON_SCOPE_SIZE, this->y+this->scope_radius, WEAPON_SCOPE_COLOR);
			hline(screen, this->x-this->scope_radius, this->y, this->x-WEAPON_SCOPE_SIZE, WEAPON_SCOPE_COLOR);
			hline(screen, this->x+WEAPON_SCOPE_SIZE, this->y, this->x+this->scope_radius, WEAPON_SCOPE_COLOR);
		}
};

int main()
{
	weapon *weapons[1];
	target *targets[TARGET_MAX_NUM];

	SAMPLE *audio_intro = NULL;
	SAMPLE *audio_main = NULL;
	SAMPLE *audio_speech = NULL;

	allegro_init();

	set_config_file("newlabour.cfg");

	set_color_depth(32);

	// if(set_gfx_mode(GFX_SAFE, 640, 480, 0, 0) < 0)
	if(set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1024, 768, 0, 0) < 0)
	{
		error *e = new error(allegro_error);
		allegro_message("Error: %s\n", e->get_message());
		quit(1);
	}

	//set_alpha_blender();
	//set_write_alpha_blender();
	//drawing_mode(DRAW_MODE_TRANS,screen,0,0);
	//text_mode(-1);

	install_keyboard();
	install_timer();

	srand(time(0));

	// if(install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL))
	if(install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL))
	{
		error *e = new error(allegro_error);
		allegro_message("Error: %s\n", e->get_message());
		quit(1);
	}

	try //load audio
	{
		audio_intro = load_sample("./wav/introloop.wav");
		if (audio_intro == NULL)
		{
			throw new error("Failed loading sample AUDIO_INTRO_LOOP");
		}

		audio_main = load_sample("./wav/mainloop.wav");
		if (audio_main == NULL)
		{
			throw new error("Failed loading sample AUDIO_MAIN_LOOP");
		}

		audio_speech = load_sample("./wav/speech.wav");
		if (audio_speech == NULL)
		{
			throw new error("Failed loading sample AUDIO_SPEECH_LOOP");
		}
	}
	catch(error *e)
	{
		allegro_message("Error: %s\n", e->get_message());
		quit(1);
	}

	while(1)
	{
		int i; //target index

		play_sample(audio_intro, 127, 127, 1000, 1);

		cls();
		splash();
		cls();

		stop_sample(audio_intro);

		play_sample(audio_main, 127, 127, 1000, 1);
		play_sample(audio_speech, 90, 127, 1000, 1);

		weapons[0] = new weapon;

		for(i=0;i<TARGET_MAX_NUM;i++)
		{
			targets[i] = new target((rand()%(SCREEN_W-TARGET_WIDTH)),(rand()%(SCREEN_H-TARGET_WIDTH)),TARGET_WIDTH,TARGET_HEIGHT,(rand()%TARGET_TOTAL_PERSONS));
			targets[i]->init();
			vsync();
			targets[i]->draw();
		}


		weapons[0]->init();
		weapons[0]->refresh();
		vsync();
		weapons[0]->draw();

		while(!key[KEY_ESC])
		{
			poll_keyboard();

			if(key[KEY_Q]) 		break;
			if(key[KEY_LEFT]) 	weapons[0]->aim_left(); //move left
			if(key[KEY_DOWN]) 	weapons[0]->aim_down(); //move down
			if(key[KEY_RIGHT]) 	weapons[0]->aim_right(); //move right
			if(key[KEY_UP]) 	weapons[0]->aim_up(); //move up
			if(key[KEY_SPACE]) 	//fire
			{
				weapons[0]->shoot();

				if(weapons[0]->target_aquired())
				{
					if(targets[weapons[0]->aquired_target]->is_hit(weapons[0]->x,weapons[0]->y))
					{
						delete targets[weapons[0]->aquired_target];
						targets[weapons[0]->aquired_target] = new target((rand()%(SCREEN_W-TARGET_WIDTH)),(rand()%(SCREEN_H-TARGET_WIDTH)),TARGET_WIDTH,TARGET_HEIGHT,(rand()%TARGET_TOTAL_PERSONS));
						targets[weapons[0]->aquired_target]->init();
						weapons[0]->unaquire_target();
					}
				}
			}

			//move the targets
			for(i=0;i<TARGET_MAX_NUM;i++) targets[i]->move();

			if(!weapons[0]->target_aquired())
			{
					for(i=0;i<TARGET_MAX_NUM;i++)
					{
						if(targets[i]->is_aquired(weapons[0]->x,weapons[0]->y))
						{
							weapons[0]->aquire_target(i);
							break;
						}
						else
						{
							weapons[0]->unaquire_target();
						}
					}

					weapons[0]->refresh();
			}
			else
			{
				weapons[0]->target_track(	(targets[weapons[0]->aquired_target]->x+(targets[weapons[0]->aquired_target]->w/2)),
												(targets[weapons[0]->aquired_target]->y+(targets[weapons[0]->aquired_target]->h/2)));
			}

			vsync();

			for(i=0;i<TARGET_MAX_NUM;i++) targets[i]->draw();

			weapons[0]->draw();

			//clear_keybuf();

			rest(9);
		}
		stop_sample(audio_main);
		stop_sample(audio_speech);
	}

	stop_sample(audio_intro);

	return quit(0);
}

END_OF_MAIN();
