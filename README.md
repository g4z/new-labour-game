
# New Labour

A simple arcade style shooter game i made some time in the early 2000's, featuring:

- blast all the major polical figures of that time with a laser canon
  - including Tony Blair, Gordon Brown and John Prescott
  - hear cool sound effects as you explode their heads

## Disclaimer

Sucks bad.

## Run on x64 Ubuntu

Clone the repo and run the pre-built binary...

```bash
$ ./newlabour
```

FileInfo:

```bash
$ file ./newlabour
newlabour: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=03b3b84c6ae7f2fc70b331eeff2b1b2ab2e23568, not stripped
```

## Build and run on Ubuntu

```bash
$ sudo apt-get install liballegro4-dev
$ make
$ ./newlabour
```

## build-on-linux.patch

```
[–]smcameron

Here's a patch that makes it work for me.

Sound doesn't work. There's tons of compiler warnings. had to "apt-get install liballegro4-dev"

It looked like the windows Makefile was building some window resource files or something. I ignored that (might be related to why sound
doesn't work.)

commit 78e65a1391d0ec86d31c898d4ebfcf27de2587cf
Author: Stephen M. Cameron <stephenmcameron@gmail.com>
Date:   Sat Aug 7 09:18:02 2021 -0400

https://www.reddit.com/r/gamedev/comments/ozqxtp/can_anyone_help_me_make_this_old_allegro_game/

```
