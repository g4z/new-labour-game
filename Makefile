
CPP  = g++
CC   = gcc
OBJ  = main.o
LINKOBJ  = main.o
BIN  = newlabour
CXXFLAGS = -Wall -DALLEGRO_STATICLINK
CFLAGS = --pedantic -Wextra
LIBS=-lalleg

.PHONY: all all-before all-after clean clean-custom

all: all-before newlabour all-after

clean: clean-custom
	rm -f ${OBJ} ${BIN}

${BIN}: ${OBJ}
	@echo LIBS = ${LIBS}
	${CPP} ${LINKOBJ} -o "newlabour" ${LIBS}

main.o: main.cpp
	${CPP} -c main.cpp -o main.o ${CXXFLAGS}

